﻿using System;

namespace Thread_.NET.BLL.Exceptions
{
    public sealed class InvalidPasswordException : Exception
    {
        public InvalidPasswordException(string message) : base($"Invalid password. Your password need to consist of {message}.") { }
    }
}
