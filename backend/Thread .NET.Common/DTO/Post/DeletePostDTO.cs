﻿using Newtonsoft.Json;

namespace Thread_.NET.Common.DTO.Post
{
    public class DeletePostDTO
    {
        [JsonIgnore]
        public int AuthorId { get; set; }

        public int Id { get; set; }
    }
}
