﻿using Newtonsoft.Json;

namespace Thread_.NET.Common.DTO.Post
{
    public class PostUpdateDTO
    {
        [JsonIgnore]
        public int AuthorId { get; set; }

        public int Id { get; private set; }

        public string Body { get; set; }
    }
}
